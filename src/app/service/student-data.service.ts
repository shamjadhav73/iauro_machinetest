import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentDataService {

  constructor(private http:HttpClient) { }


  stdDataNew = [
    {id: 1, name: 'Hydrogen N', std:'Bsc', gender:'Male', txt:'Pune Abc Chowk', subject:'Biology'},
    {id: 2, name: 'Hydrogen', std:'BA', gender:'Male', txt:'Pune -Shivaji nager', subject:'Biology'},
   
  ];
  
  getWheaterINfo(){
    const headers = new HttpHeaders({
      // "Authorization": localStorage.getItem('token')
    })
    return this.http.get(`${environment.apiUrl}&APPID=681a6077e33e4a08f8a724fc50186959`, { headers: headers });
  }

}
