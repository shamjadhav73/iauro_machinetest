import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StudentDataService } from '../service/student-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  editStudentInfo: FormGroup;
  getAllINfo;
  allData;
  getSubjValue;
  id;
  getallData;
  getData;
  gender: string[] = ['Male', 'Female'];
  csubject: string[] = ['Biology'];
  qualification: string[] = ['Bsc', 'BA', 'Bcom'];

  constructor(private fb: FormBuilder,
    private sservice: StudentDataService,
    private router: ActivatedRoute,
    private route: Router,
    public location: Location

  ) { }

  ngOnInit() {
    this.id = this.router.snapshot.params.id;
    this.getAllData();
  }

  getAllData() {
    this.getallData = this.sservice.stdDataNew;
    this.getallData.forEach((user) => {
      if (user.id == this.id) {
        this.getData = user;
        console.log(this.getData);
      }
    })
    // console.log();
    this.customForm(this.getData);
  }

  customForm(data) {


    this.editStudentInfo = this.fb.group({
      name: [data.name, Validators.required],
      std: [data.std, Validators.required],
      gender: [data.gender, Validators.required],
      txt: [data.txt, Validators.required],
      subject: [data.subject, Validators.required]
    })
  }


  onCheckChange(event) {
    this.getSubjValue = event.source.value;
  }

  onsubmit() {
    this.getAllINfo = this.sservice.stdDataNew;
    let index = this.getAllINfo.findIndex(x => x.id == this.id);
    this.getAllINfo.splice(index, this.id);
    console.log( this.getAllINfo);

    this.allData = {
      id: Number(this.id),
      name: this.editStudentInfo.value.name,
      std: this.editStudentInfo.value.std,
      gender: this.editStudentInfo.value.gender,
      txt: this.editStudentInfo.value.txt,
      subject: this.getSubjValue
    }
    console.log(this.allData)
    this.sservice.stdDataNew.push(this.allData)
    this.route.navigate(['../../list'], { relativeTo: this.router })

  }

}
