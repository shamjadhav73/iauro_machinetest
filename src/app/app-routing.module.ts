import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';
import { StudentListComponent } from './student-list/student-list.component';


const routes: Routes = [
  { path: '', component: StudentListComponent },
  { path: 'list', component: StudentListComponent },
  { path: 'edit/:id', component: EditComponent },
  { path: 'add', component: AddComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


export const routingComponent = [
  StudentListComponent, EditComponent , AddComponent
]