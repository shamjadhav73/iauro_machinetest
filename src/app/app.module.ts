import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatTableModule} from '@angular/material/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatPaginatorModule, MatSortModule, MatInputModule, MatSelectModule, MatRadioModule, MatRadioButton, MatRadioGroup, MatCheckboxModule } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


// import {MatRadioModule, MatRadioButton, MatRadioGroup} from '@angular/material/radio';


@NgModule({
  declarations: [
    AppComponent,
    routingComponent
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatRadioModule,
  MatCheckboxModule,
  HttpClientModule

  ],
  
  providers: [],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
