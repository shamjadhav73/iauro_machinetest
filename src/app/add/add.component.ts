import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { MatPaginator, MatSort } from '@angular/material';
import { StudentDataService } from '../service/student-data.service';
import { Router, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  addStudentInfo: FormGroup;
  getSubjValue;
  getAllINfo;

  gender: string[] = ['Male', 'Female'];
  csubject: string[] = ['Biology'];
  qualification: string[] = ['Bsc', 'BA', 'Bcom'];
  allData;

  constructor(
    private fb: FormBuilder,
    private sservice: StudentDataService,
    private router: Router,
    private route: ActivatedRoute,
    public location:Location

  ) { }

  ngOnInit() {
    this.customForm();

  }

  onCheckChange(event) {
    this.getSubjValue = event.source.value;
  }
  customForm() {
    this.addStudentInfo = this.fb.group({
      name: ['', Validators.required],
      std: ['', Validators.required],
      gender: ['', Validators.required],
      txt: ['', Validators.required],
      subject: ['', Validators.required]
    })
  }



  onsubmit() {
    this.getAllINfo = this.sservice.stdDataNew;
    let id = [];

    this.getAllINfo.forEach((user) => {
      id.push(user.id);
    })
  
    let newId = id.slice(-1)[0]+1;

    this.allData = {
      id: newId,
      name: this.addStudentInfo.value.name,
      std: this.addStudentInfo.value.std,
      gender: this.addStudentInfo.value.gender,
      txt: this.addStudentInfo.value.txt,
      subject: this.getSubjValue
    }
    // console.log(allData)
    this.sservice.stdDataNew.push(this.allData)
    this.router.navigate(['../list'], { relativeTo: this.route })

  }
}
