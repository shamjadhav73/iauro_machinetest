import { Component, OnInit } from '@angular/core';
import { StudentDataService } from '../service/student-data.service';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  constructor(
    private dataService: StudentDataService, private router: Router, private route: ActivatedRoute,
    ) { }
  getAllINfo;
  dataSource;
  whetherData;
  whetherDeg;


  ngOnInit() {
    this.getAllData();
   console.log( localStorage.getItem('data'));
    this.getWhether();
  }

  getAllData() {
    this.getAllINfo = this.dataService.stdDataNew;
    this.dataSource = this.getAllINfo;
    console.log(this.dataSource);
  }

  deleteStudent(id) {
    if(confirm('Are you sure do you want to delete this record')){
      let index = this.getAllINfo.findIndex(x => x.id == id);
      this.getAllINfo.splice(index, id);
    }
    
  }

  editStudent(id) {
    this.router.navigate(['../edit', id], { relativeTo: this.route })
  }

  getWhether() {
    this.dataService.getWheaterINfo().subscribe(res => {
    //  let demo = JSON.stringify(res)
    this.whetherData = res['wind'];
     this.whetherDeg = res['main'];
    console.log( )
    })
  }
}
